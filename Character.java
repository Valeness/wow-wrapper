package com.valeness;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class Character extends WoW {

    private String name;
    private String realm;
    public Item[] equipment;

    /**
     *
     * @param name Name of character you wish to return
     */
    Character(String name) {
        super();
        this.name = name;
        this.realm = "test";
    }

    /**
     *
     * @param realm Overrides default realm for calls that require it
     * @return Returns an instance of iteself to be used with builder method - ala - new Character("name").realm("realm")
     */
    public Character realm(String realm) {
        this.realm = realm;
        return this;
    }

    private JSONObject get(ArrayList<String[]> get_fields) {
        return Http.call(this.buildUrl("/character/" + this.realm + "/" + this.name, get_fields ));
    }

    /**
     *
     * @return Returns basic character info for this.name. Like name, kills, race, class, etc.
     * TODO:: Return class/race names instead of ids
     */
    public JSONObject getInfo() {

        JSONObject items = this.getEquippedItems();
        items = items.getJSONObject("items");
        Iterator<?> keys = items.keys();

        int curr_index = 0;

        this.equipment = new Item[items.length()];

        while(keys.hasNext()) {
            String key = (String)keys.next();
            if(items.get(key) instanceof JSONObject) {
                JSONObject item = items.getJSONObject(key);
                Item item_slot = new Item(item.getInt("id"), item.getString("name"), key);
                this.equipment[curr_index] = item_slot;
                curr_index++;
            }
        }

        return this.get(new ArrayList<>());
    }

    private JSONObject getSubInfo(String field) {
        ArrayList<String[]> get_fields = new ArrayList<>();
        String[] fields = {"fields", field};
        get_fields.add(fields);

        return this.get(get_fields);
    }

    /**
     *
     * @return Returns Equipped Items for character this.name
     */
    public JSONObject getEquippedItems() {



        return this.getSubInfo("items");
    }

    public JSONObject getAchievements() {
        return this.getSubInfo("achievements");
    }

    public JSONObject getProfessions() {
        return this.getSubInfo("professions");
    }


}
