package com.valeness;
import org.json.JSONObject;

import java.util.Iterator;

public class Main {

    /**
     * Test function for showing how the API wrapper may be used
     */
    public static void main(String[] args) {

        WoW wow = new WoW();
        JSONObject races = wow.getRaces();

        Character valeness = new Character("valeness").realm("dalaran");
        JSONObject info = valeness.getInfo();
        print(info.get("name"));
        print(info.get("realm"));

        for (Item item: valeness.equipment) {
            if(item != null) {
                print(item.name);
            }
        }

//        JSONObject achievements = valeness.getAchievements();
//        JSONObject professions = valeness.getProfessions();

//        JSONObject equipped_items = valeness.getEquippedItems().getJSONObject("items");
//        print(equipped_items.getJSONObject("head").get("name"));
//
//        Iterator<?> keys = equipped_items.keys();
//
//        while(keys.hasNext()) {
//            String key = (String)keys.next();
//            if(equipped_items.get(key) instanceof JSONObject) {
//                Object item_name = equipped_items.getJSONObject(key).get("name");
//                print("Slot: " + key + " - " + item_name);
//            }
//        }

    }

    /**
     * Helper for System.out - Hate typing it out all the time
     * Prints a string
     * @param str String to be printed
     */
    private static void print(Object str) {
        System.out.println(str);
    }

}
