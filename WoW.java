package com.valeness;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@SuppressWarnings({"FieldCanBeLocal"})
class WoW {

    private String api_token = "9dkq5uywm47bbcbw9cmc2tedrhybnr9b";
    private String uri = "https://us.api.battle.net/wow";

    public JSONObject getRaces() {
        return Http.call(this.buildUrl("/data/character/races", new ArrayList<>()));
    }

    /**
     *
     * @param slug API Endpoint, must contain leading forward slash '/'
     * @param get_fields Any GET parameters the call requires ?hello=world
     * @return Constructs slug and get_fields in a way that is agreeable to Http.call
     */
    String buildUrl(String slug, List<String[]> get_fields) {

        String[] api_add = {"apikey", this.api_token};
        get_fields.add(api_add);

        ArrayList<String> total_gets = new ArrayList<String>();
        for (String[] field: get_fields) {
            total_gets.add(field[0] + '=' + field[1]);
        }

        return this.uri + slug + "?" + total_gets.stream().map(Object::toString).collect(Collectors.joining("&"));
    }

}
