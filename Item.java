package com.valeness;

@SuppressWarnings("WeakerAccess")
public class Item {
    public final String body_part;
    public final String name;

    Item(int id, String name, String body_part) {
        super();
        this.body_part = body_part;
        this.name = name;
    }

}
