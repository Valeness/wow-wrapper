package com.valeness;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class Http {

    /**
     * Make an HTTP Call to a JSON endpoint
     * @param uri Full url to be called. Is not WoW Specific
     * @return Returns a JSONObject containing the http response
     */
    public static JSONObject call(String uri) {
        URL url = null;
        try {
            url = new URL(uri);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        URLConnection c = null;
        try {
            assert url != null;
            c = url.openConnection();
            c.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
        } catch (IOException e) {
            e.printStackTrace();
        }

        InputStreamReader isr = null;
        try {
            assert c != null;
            isr = new InputStreamReader(c.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert isr != null;
        BufferedReader in = new BufferedReader(isr);

        String inputLine;

        StringBuilder sb = new StringBuilder();

        try {
            while ((inputLine = in.readLine()) != null) {
                sb.append(inputLine);
            }
        } catch(IOException e) {
            e.printStackTrace();
        }

        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new JSONObject(sb.toString());
    }

}
